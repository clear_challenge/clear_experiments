'''
    Absolute path where you clone the avalanche library
'''
AVALANCHE_PATH = "/work/repos/avalanche"


'''
    Where to save the datasets/models
'''
ROOT = "/work/data/clear_benchmark" # Where to store datasets

'''
    Which dataset to train on
'''
DATASET_NAME = "clear10"
#DATASET_NAME = "clear100_cvpr2022"
