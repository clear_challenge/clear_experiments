import os
import json
from pathlib import Path
import copy
import sys
import logging
from typing import Type, Any, Callable, Union, List, Optional

import random
import numpy as np
import torch
from torch import nn as nn, Tensor
import torch.nn.functional as F
import PIL
import torchvision
from torchvision.models.resnet import (
    BasicBlock,
    Bottleneck,
    ResNet
    )
from torchvision import transforms as T

from . import console_colors as ccl

from avalanche.benchmarks.utils.dataset_utils import (
    manage_advanced_indexing,
    SequenceDataset,
    ClassificationSubset,
    LazyConcatIntTargets,
    find_list_from_index,
    ConstantSequence,
    LazyClassMapping,
    optimize_sequence,
    SubSequence,
    LazyConcatTargets,
    TupleTLabel,
)
from typing import (
    List,
    Any,
    Sequence,
    Union,
    Optional,
    TypeVar,
    SupportsInt,
    Callable,
    Dict,
    Tuple,
    Collection,
)
from avalanche.evaluation.metrics import (
    forgetting_metrics,
    accuracy_metrics,
    loss_metrics,
    timing_metrics,
    cpu_usage_metrics,
    confusion_matrix_metrics,
    disk_usage_metrics,
)
from avalanche.logging import InteractiveLogger, TextLogger, TensorboardLogger, WandBLogger
from avalanche.training.plugins import (EvaluationPlugin, 
    ReplayPlugin, EWCPlugin, SupervisedPlugin)
from avalanche.training.plugins.lr_scheduling import LRSchedulerPlugin
from avalanche.training.supervised import Naive
from avalanche.training.storage_policy import (
    ExemplarsBuffer,
    ExperienceBalancedBuffer,
)
from avalanche.benchmarks.utils.avalanche_dataset import AvalancheDataset
from avalanche.benchmarks.utils.data_loader import ReplayDataLoader
from avalanche.benchmarks.classic.clear import CLEAR, CLEARMetric


#++++++++++++++++++++++++++++++++++++++++++
# MODELS
#++++++++++++++++++++++++++++++++++++++++++

class ResnetWithDropout(ResNet):

    def __init__(
        self,
        block: Type[Union[BasicBlock, Bottleneck]],
        layers: List[int],
        num_classes: int = 1000,
        zero_init_residual: bool = False,
        groups: int = 1,
        width_per_group: int = 64,
        replace_stride_with_dilation: Optional[List[bool]] = None,
        norm_layer: Optional[Callable[..., nn.Module]] = None,
    ) -> None:

        super().__init__(block, layers, num_classes, zero_init_residual, groups, 
            width_per_group, replace_stride_with_dilation, norm_layer)

        self.dropout = nn.Dropout(0.25)

    def forward(self, x: Tensor) -> Tensor:
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        if self.training:
            x = self.dropout(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        if self.training:
            x = self.dropout(x)

        return x


#++++++++++++++++++++++++++++++++++++++++++
# DATASETS
#++++++++++++++++++++++++++++++++++++++++++

class AugmentationDataset(AvalancheDataset):
    """ DaFaq boy...
    """

    def __init__(self, dataset, *args, **kws):
        """ yeap...
        """
        for k in ('fraction_augmentation', 'sporadic_transform'):
            assert k in kws, \
                '[!] Must specify "%s" argument!' % k


        #--- insert augmentation transform
        self._set_sporadic_transform(kws.pop('sporadic_transform'))
        self._mutate_sample = False
        self.normalize = T.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
            )

        self.fraction_augmentation = kws.pop('fraction_augmentation')
        # inherit everything else
        super().__init__(dataset, *args, **kws)
        #--- define sample parameters
        self.reset_random_indexes()
        self._transform_orig = copy.deepcopy(self._dataset.transform)

    def reset_random_indexes(self):
        # TODO: perhaps call this method in every iteration? from the plugin.
        #n = dataset.tensors[0].shape[0]  # total number of samples
        n = len(self)
        n_sample = int(self.fraction_augmentation * n)  #kws.pop('n_sample')
        #n = len(dataset)
        self.indexes_sample = set(random.sample(
            np.arange(n).tolist(), k=n_sample))

    def _set_sporadic_transform(self, sporadic_transform):
        self.sporadic_transform = sporadic_transform

    def _get_single_item(self, idx: int):
        self._mutate_sample = (idx in self.indexes_sample)
        assert self._dataset.transform is not None
        if self._mutate_sample: #and (self._dataset.transform is not None):
            #print('>>> USING TRANSFORM FOR IMAGE ...')
            self._dataset.transform.transforms.insert(0, self.sporadic_transform)
        else:
            self._dataset.transform = copy.deepcopy(self._transform_orig)

        assert self._dataset.transform is not None
        return self._process_pattern(self._dataset[idx], idx)


#++++++++++++++++++++++++++++++++++++++++++
# PLUGINS
#++++++++++++++++++++++++++++++++++++++++++

class ReplayAugmentationPlugin(SupervisedPlugin):

    def __init__(
        self,
        mem_size: int = 200,
        batch_size: int = None,
        batch_size_mem: int = None,
        task_balanced_dataloader: bool = False,
        storage_policy: Optional["ExemplarsBuffer"] = None,
        fraction_augmentation = 0.1,
    ):
        super().__init__()
        self.mem_size = mem_size
        self.batch_size = batch_size
        self.batch_size_mem = batch_size_mem
        self.task_balanced_dataloader = task_balanced_dataloader
        self.fraction_augmentation = fraction_augmentation

        if storage_policy is not None:  # Use other storage policy
            self.storage_policy = storage_policy
            assert storage_policy.max_size == self.mem_size
        else:  # Default
            self.storage_policy = ExperienceBalancedBuffer(
                max_size=self.mem_size, adaptive_size=True
            )

        # define augmentation transform
        self.sporadic_transform = T.RandAugment(num_ops=2, magnitude=4,)
        print('+++++++++++++++++++++ CUSTOM PLUGIN ++++++++++++++++++++++++')
        print('+++++++++++++++++++++ CUSTOM PLUGIN ++++++++++++++++++++++++')
        print('+++++++++++++++++++++ CUSTOM PLUGIN ++++++++++++++++++++++++')

    @property
    def ext_mem(self):
        print('+++++++++++ GET EXT MEM (buffer groups) ++++++++++++++++++++++++')
        return self.storage_policy.buffer_groups  # a Dict<task_id, Dataset>

    def before_training_exp(
        self,
        strategy: "SupervisedTemplate",
        num_workers: int = 0,
        shuffle: bool = True,
        **kwargs
    ):
        """
        Dataloader to build batches containing examples from both memories and
        the training dataset
        """
        if len(self.storage_policy.buffer) == 0:
            # first experience. We don't use the buffer, no need to change
            # the dataloader.
            print('+++++++++++ STORAGE EMPTY ++++++++++++++++++++++++')
            return

        print('+++++++++++ STORAGE NON-EMPTY ++++++++++++++++++++++++')
        batch_size = self.batch_size
        if batch_size is None:
            batch_size = strategy.train_mb_size

        batch_size_mem = self.batch_size_mem
        if batch_size_mem is None:
            batch_size_mem = strategy.train_mb_size

        data = {
            'present': AugmentationDataset(strategy.adapted_dataset, 
                fraction_augmentation = self.fraction_augmentation,
                sporadic_transform = self.sporadic_transform,
                ),
            'memory': AugmentationDataset(self.storage_policy.buffer,
                fraction_augmentation = self.fraction_augmentation,
                sporadic_transform = self.sporadic_transform,
                ),
            }
        strategy.dataloader = ReplayDataLoader(
            data['present'],
            data['memory'],
            oversample_small_tasks = True,
            batch_size = batch_size,
            batch_size_mem = batch_size_mem,
            task_balanced_dataloader = self.task_balanced_dataloader,
            num_workers = num_workers,
            shuffle = shuffle,
        )

    def before_training_epoch(self, strategy):
        if len(self.storage_policy.buffer) > 0:
            print('>>> NOW RESETING SAMPLE INDEXES...')
            strategy.dataloader.data.reset_random_indexes()
            strategy.dataloader.memory.reset_random_indexes()

    def after_training_exp(self, strategy: "SupervisedTemplate", **kwargs):
        print('+++++++++++ AFTER-TRAINING-EXP ++++++++++++++++++++++++')
        self.storage_policy.update(strategy, **kwargs)


#++++++++++++++++++++++++++++++++++++++++++
# LOSSES
#++++++++++++++++++++++++++++++++++++++++++

class LabelSmoothingCrossEntropy(nn.Module):
    """
    NLL loss with label smoothing.
    """
    def __init__(self, smoothing=0.1):
        """
        Constructor for the LabelSmoothing module.
        :param smoothing: label smoothing factor
        """
        super(LabelSmoothingCrossEntropy, self).__init__()
        assert smoothing < 1.0
        self.smoothing = smoothing
        self.confidence = 1. - smoothing

    def forward(self, x, target):
        logprobs = F.log_softmax(x, dim=-1)
        nll_loss = -logprobs.gather(dim=-1, index=target.unsqueeze(1))
        nll_loss = nll_loss.squeeze(1)
        smooth_loss = -logprobs.mean(dim=-1)
        loss = self.confidence * nll_loss + self.smoothing * smooth_loss
        return loss.mean()


#++++++++++++++++++++++++++++++++++++++++++
# SPECIFIC RESNETs
#++++++++++++++++++++++++++++++++++++++++++


def _resnet(
    arch: str,
    block: Type[Union[BasicBlock, Bottleneck]],
    layers: List[int],
    pretrained: bool,
    progress: bool,
    **kwargs: Any,
) -> ResNet:
    model = ResnetWithDropout(block, layers, **kwargs)
    if pretrained:
        state_dict = load_state_dict_from_url(model_urls[arch], progress=progress)
        model.load_state_dict(state_dict)
    return model


def resnet_with_dropout(arch='resnet18', pretrained: bool = False, progress: bool = True, **kwargs: Any) -> ResNet:
    r"""ResNet-18 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    if arch == 'resnet18':
        return _resnet("resnet18", BasicBlock, [2, 2, 2, 2], pretrained, progress, **kwargs)
    elif arch == 'resnet34':
        return _resnet("resnet34", BasicBlock, [3, 4, 6, 3], pretrained, progress, **kwargs)
    else:
        raise Exception()


#EOF
