# CLEAR Experiments

To easily test this project, open a terminal, change directory to the repository root path and:

    python experim/test.py -c "./experim/config.yaml"


The config file [config.yaml](./experim/config.yaml) contains specifications about hyper-parameters, procedure, and whether to enable [wandb logging](https://wandb.ai/site) or not.

The config file [procedures.yaml](./experim/procedures.yaml) is used to define a specific combination of ingredients (i.e. model, CL strategy, plugins, optimizer, loss function, etc) that make sense to test.
This is useful to log any combination of thing we want to try.

---
## Custom Models, Plugins, Losses, Policies and Dataset classes

The [custom_models.py](./experim/custom_models.py) contain set of custom models, plugins, losses and dataset classes. It was mainly inspired wanting to use an augmentation method ("RandAugment" specifically) and understanding Avalanche design.

The [custom_policies.py](./experim/custom_policies.py) contains custom storage policies. For replay memory handling stuff.

<!--- EOF -->
