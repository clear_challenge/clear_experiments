import sys
from typing import List, TYPE_CHECKING, Tuple, Type

import torch

from avalanche.core import SupervisedPlugin
from avalanche.evaluation.metric_results import MetricValue, TensorImage
from avalanche.logging import BaseLogger, TextLogger
from avalanche.evaluation.metric_utils import stream_type, phase_and_task


class CustomTextLogger(TextLogger):

    def __init__(self, file=sys.stdout):
        """
        Creates an instance of `TextLogger` class.

        :param file: destination file to which print metrics
            (default=sys.stdout).
        """
        super().__init__()
        self.file = file
        self.metric_vals = {}

        # mb losses
        self.mb_losses = []
        self._LOSS_NAME_PREFIX = 'Loss_MB/train_phase/train_stream'

    def after_training_iteration(
        self,
        strategy: "SupervisedTemplate",
        metric_values: List["MetricValue"],
        **kwargs
    ):
        # grab loss of this mini-batch (iteration) 
        self.mb_losses.append(strategy.loss.item())
        #self.print_current_metrics()

        self.metric_vals = {}
        super().after_training_iteration(strategy, metric_values, **kwargs)

    def after_training_epoch(
        self,
        strategy: "SupervisedTemplate",
        metric_values: List["MetricValue"],
        **kwargs,
    ):
        super().after_training_epoch(strategy, metric_values, **kwargs)
        #import pdb; pdb.set_trace()
        print(
            f"Epoch {strategy.clock.train_exp_epochs} ended.",
            file=self.file,
            flush=True,
        )
        self.print_current_metrics()
        self.metric_vals = {}

    def before_training_exp(
        self,
        strategy: "SupervisedTemplate",
        metric_values: List["MetricValue"],
        **kwargs,
    ):
        super().before_training_exp(strategy, metric_values, **kwargs)
        self._on_exp_start(strategy)
        self.mb_losses = []

#EOF
