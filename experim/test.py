#!/usr/bin/env python
'''Training and evaluating on CLEAR benchmark (RGB images)
'''
import json, yaml
from pathlib import Path
import copy
import argparse
import re
import os, sys
import logging
import config
# IMPORTANT! Need to add avalanche to sys path
if config.AVALANCHE_PATH:
    print(f"Importing avalanche library path {config.AVALANCHE_PATH} to sys.path")
    sys.path.append(config.AVALANCHE_PATH)
else:
    print("Please specify avalanche library path in config.py")
    exit(0)

import numpy as np
import pandas as pd
import torch
import torchvision
from torchvision import transforms as T
import wandb
import console_colors as ccl
import importlib
torch.cuda.empty_cache()

from avalanche.evaluation.metrics import (
    forgetting_metrics,
    accuracy_metrics,
    loss_metrics,
    timing_metrics,
    cpu_usage_metrics,
    confusion_matrix_metrics,
    disk_usage_metrics,
)
from avalanche.logging import InteractiveLogger, TextLogger, TensorboardLogger, WandBLogger
from avalanche.training.plugins import (EvaluationPlugin, 
    ReplayPlugin, EWCPlugin)
from avalanche.training.plugins.lr_scheduling import (
    LRSchedulerPlugin,
    )
from avalanche.training.supervised import (
    Naive, Cumulative, StreamingLDA, ICaRL)
from avalanche.benchmarks.classic.clear import CLEAR, CLEARMetric

from avalanche.models import IcarlNet, make_icarl_net, initialize_icarl_net

import experim.custom_models as custom
from experim import custom_policies
from experim.custom_loggers import CustomTextLogger

#--- loggers
#logging.root.handlers = []
def build_logger(fname_log):
    """
    https://stackoverflow.com/questions/13733552/logger-configuration-to-log-to-file-and-print-to-stdout
    """
    root_logger = logging.getLogger('.')
    root_logger.setLevel(logging.INFO)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")

    # file handler
    fileHandler = logging.FileHandler(fname_log, mode='w',) #delay=False)
    #fileHandler.level = logging.INFO
    fileHandler.setFormatter(logFormatter)

    # console handler
    consoleHandler = logging.StreamHandler()
    #consoleHandler.level = logging.INFO
    consoleHandler.setFormatter(logFormatter)

    root_logger.addHandler(fileHandler)
    root_logger.addHandler(consoleHandler)
    return root_logger


#--- etc
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

#--- YAML parser
# SOURCE: https://stackoverflow.com/questions/30458977/yaml-loads-5e-6-as-string-and-not-a-number
my_yaml_loader = yaml.SafeLoader
my_yaml_loader.add_implicit_resolver(
    u'tag:yaml.org,2002:float',
    re.compile(u'''^(?:
     [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
    |[-+]?\\.(?:inf|Inf|INF)
    |\\.(?:nan|NaN|NAN))$''', re.X),
    list(u'-+0123456789.'))

#data = yaml.load(jAll, Loader=loader)


def get_dataset(train_stream, index, HPARAM):
    data_set = train_stream[index].dataset.train()
    train_loader = torch.utils.data.DataLoader(data_set, 
                              batch_size=HPARAM['batch_size'], shuffle=True)
    return train_loader


def train(loader, model, HPARAM, loss_fn, optim_name='sgd'):

    # TODO: what if I get this 'get_optimizer_schedular()' line out of
    # the "experiences loop"?? The 'cl_strategy' contains these anyway (might
    # indicate something about the immutability of those objs).
    optimizer, scheduler = get_optimizer_schedular(model, HPARAM, optim_name)

    n_epochs = HPARAM['num_epoch']
    losses = []
    for epoch in range(n_epochs):
        acc_ = 0
        for _, data in enumerate(loader):
            input, target, _ = data
            optimizer.zero_grad()
            input = input.cuda()
            target = target.cuda()
            pred = model(input)
            loss = loss_fn(pred, target)
            loss.backward()
            # print(loss)
            losses.append(loss.item())

            acc_ += (torch.sum(torch.eq(torch.max(pred, 1)[1], 
                               target)) / len(pred)).item()
            optimizer.step()
        acc_ = acc_/len(loader)
        logger.info(f'training accuracy for epoch {epoch} is {acc_}')
        # TODO: append to accuracies to W&B!
        scheduler.step()
    # TODO: check if 'model' has still the same pointer!
    return losses


def train2(cl_strategy, data_loader):
    cl_strategy.train(data_loader)


def get_optimizer_schedular(model, HPARAM, optim_name, scheduler_method):
    if optim_name == 'sgd':
        optimizer = torch.optim.SGD(
            model.parameters(),
            lr = HPARAM["start_lr"],
            weight_decay = HPARAM["weight_decay"],
            momentum = HPARAM["momentum"],
            )
    elif optim_name == 'adam':
        optimizer = torch.optim.Adam(
            model.parameters(),
            lr = HPARAM['start_lr'],
            betas = (0.9, 0.999),
            eps = 1e-8,
            weight_decay = HPARAM['weight_decay'],
            amsgrad = False,
            )
    else:
        raise Exception('Unexpected optimizer name! (%s)' % optim_name)

    scheduler = make_scheduler(
        optimizer,
        params = HPARAM,
        method = scheduler_method,
        )
    return optimizer, scheduler


def make_scheduler(optimizer, params, method='step-lr'):
    # TODO::idea: algun otro scheduler mas complejo o inteligente???
    if method == 'step-lr':
        scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer, 
            step_size = params['step-lr/step_size'],
            gamma = params['step-lr/gamma'],
            )
    elif method == 'cosine-annealing':
        # SGDR: Stochastic Gradient Descent with Warm Restarts:
        # https://arxiv.org/abs/1608.03983
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
            optimizer,
            T_max = params['cosine-annealing/T_max'],
            )
    return scheduler


class RunMgr:

    def __init__(self, hparams, model_name, strategy, optim_name, loss_name, plugins, 
            scheduler, wandb_opts, dir_dst):

        # For CLEAR dataset setup
        self.NUM_CLASSES = {"clear10": 11, "clear100_cvpr2022": 100}
        assert config.DATASET_NAME in self.NUM_CLASSES.keys()

        # please refer to paper for discussion on streaming v.s. iid protocol
        self.EVALUATION_PROTOCOL = "streaming"  # trainset = testset per timestamp
        # self.EVALUATION_PROTOCOL = "iid"  # 7:3 trainset_size:testset_size


        self.model_name = model_name
        self.strategy = strategy
        self.loss_name = loss_name
        self.plugins = plugins
        self.scheduler = scheduler

        # Paths for saving datasets/models/results/log files
        logger.info(
            f"The dataset/models will be saved at {Path(config.ROOT).resolve()}. "
            f"You may change this path in config.py."
        )
        #ROOT = Path(config.ROOT)
        DATA_ROOT = Path(config.ROOT) / config.DATASET_NAME
        MODEL_ROOT = Path(dir_dst) / "models"
        DATA_ROOT.mkdir(parents=True, exist_ok=True)
        MODEL_ROOT.mkdir(parents=True, exist_ok=True)
        self.paths = {
            'dir_dst': Path(dir_dst),
            'data': DATA_ROOT,
            'model': MODEL_ROOT
            }

        self.HPARAM = hparams
        self.optim_name = optim_name

        # init W&B
        self.use_wandb = wandb_opts.get('enable', False)
        if self.use_wandb:
            wandb.init(
                job_type = wandb_opts.get('job_type', None),
                project = wandb_opts['project'],
                name = wandb_opts['name_run'],
                id = wandb_opts.get('id', None),
                tags = wandb_opts.get('tags', None),
                config = wandb_opts.get('config', None),
                )


    def run(self):
        self._set_model()
        self._set_evaluation_plugin()
        self._define_loss_function()
        self.define_strategy()
        self._set_misc()
        self.set_scenario()
        self.train_eval_on_experiences()
        self._calc_metrics()
        self._save_metrics()
        
        if self.use_wandb:
            wandb.finish()


    def _set_model(self):
        if self.model_name == 'resnet18':
            self.model = torchvision.models.resnet18(pretrained=False).to(device)

        elif self.model_name == 'resnet34':
            self.model = torchvision.models.resnet34(pretrained=False).to(device)

        elif self.model_name == 'resnet50':
            self.model = torchvision.models.resnet50(pretrained=False).to(device)

        elif self.model_name == 'resnet152':
            self.model = torchvision.models.resnet152(pretrained=False).to(device)

        elif self.model_name == 'resnet18_with_dropout':
            self.model = custom.resnet_with_dropout(
                arch='resnet18', pretrained=False
                ).to(device)

        elif self.model_name == 'icarl':
            self.model = IcarlNet(
                num_classes = 11,
                n = 3,  # default: 5
                c = 3,  # default: 3
                resblock_kind = 'a',
                ).to(device)

        else:
            raise Exception('Unexpected model name (%s)!' % self.model_name)

        if self.use_wandb:
            wandb.watch(self.model, log='all')


    def _set_evaluation_plugin(self):

        #--- loggers
        # log to text file
        self.text_logger = CustomTextLogger(open(self.paths['dir_dst'] / "log.txt", "w+"))
        # print to stdout
        interactive_logger = InteractiveLogger()

        #--- EVLUATION PLUGIN
        self.eval_plugin = EvaluationPlugin(
            accuracy_metrics(minibatch=True, epoch=True, experience=True, 
                stream=True),
            loss_metrics(minibatch=True, epoch=True, experience=True, 
                stream=True),
            timing_metrics(epoch=True, epoch_running=True),
            forgetting_metrics(experience=True, stream=True),
            cpu_usage_metrics(experience=True),
            confusion_matrix_metrics(
                num_classes = self.NUM_CLASSES[config.DATASET_NAME], 
                save_image=False, stream=True
                ),
            disk_usage_metrics(
                minibatch=True, epoch=True, experience=True, stream=True
                ),
            loggers=[interactive_logger, self.text_logger],
            )


    def define_strategy(self):

        #--- OPTIMIZER & SCHEDULER
        optimizer, scheduler = get_optimizer_schedular(
            self.model, self.HPARAM, self.optim_name, self.scheduler)


        #--- TRAINING PLUGINS
        aug_replay_storage_policy = custom_policies.ExperienceBalancedBuffer(
            max_size=200, adaptive_size=True)
        plugin_coll = {
            'lr_scheduler': LRSchedulerPlugin(scheduler),
            'replay': ReplayPlugin(
                batch_size_mem = self.HPARAM.get('replay/batch_size_mem', 10)),
            'ewc': EWCPlugin(
                ewc_lambda = 0.001, 
                mode = "separate"),
            'augmented-replay': custom.ReplayAugmentationPlugin(
                batch_size_mem = self.HPARAM.get('augmented-replay/batch_size_mem', 10), 
                fraction_augmentation = self.HPARAM.get('augmented-replay/fraction_augmentation', 0.1),
                storage_policy = aug_replay_storage_policy),
            }
        # check that the plugins specified in config YAML are included in the implementations here.
        if any(p not in plugin_coll for p in self.plugins):
            logger.info('There\'s at least one plugin you specified that was unexpected!')
            plist_str = '\n > '.join(list(plugin_coll))
            logger.info('This is the list of accepted plugins:\n > %s' % plist_str)
            raise Exception()

        self.plugin_list = []
        for plugin in self.plugins:
            self.plugin_list.append(plugin_coll[plugin])


        #--- TRAINING STRATEGY
        if self.strategy == 'naive':
            self.cl_strategy = Naive(
                self.model,
                optimizer,
                criterion = self.loss_fn,
                train_mb_size = self.HPARAM["batch_size"],
                train_epochs = self.HPARAM["num_epoch"],
                eval_mb_size = self.HPARAM["batch_size"],
                evaluator = self.eval_plugin,
                device = device,
                plugins = self.plugin_list,
                )

        elif self.strategy == 'cumulative':
            self.cl_strategy = Cumulative(
                self.model,
                optimizer,
                criterion = self.loss_fn,
                train_mb_size = self.HPARAM["batch_size"],
                train_epochs = self.HPARAM["num_epoch"],
                eval_mb_size = self.HPARAM["batch_size"],
                evaluator = self.eval_plugin,
                device = device,
                plugins = self.plugin_list,
                )

        elif self.strategy == 'slda':
            """ Doesn't work. Need to read the paper.
            """
            self.cl_strategy = StreamingLDA(
                self.model,
                criterion = self.loss_fn,
                input_size = 1000, #10, #5, #30, #15, #args.feature_size,
                num_classes = 10,
                eval_mb_size = self.HPARAM["batch_size"],
                train_mb_size = self.HPARAM["batch_size"],
                train_epochs = self.HPARAM['num_epoch'],
                shrinkage_param = 1e-4,
                streaming_update_sigma = True, #args.plastic_cov,
                device = device,
                )

        elif self.strategy == 'icarl':
            assert isinstance(self.model, IcarlNet)
            self.cl_strategy = ICaRL(
                self.model.feature_extractor,
                self.model.classifier,
                optimizer = optimizer,
                memory_size = self.HPARAM.get('icarl/memory_size', 1_000),
                #buffer_transform = transforms.Compose([icarl_cifar100_augment_data]), # TODO: (*)
                buffer_transform = None,
                fixed_memory = True,
                train_mb_size = self.HPARAM['batch_size'],
                train_epochs = self.HPARAM['num_epoch'],
                eval_mb_size = self.HPARAM['batch_size'],
                plugins = self.plugin_list,
                device = device,
                #evaluator=evaluator,
                )
            # (*): maybe we can specify a list of augmentation techniques here.

        else:
            raise Exception()


    def set_scenario(self):
        if self.EVALUATION_PROTOCOL == "streaming":
            seed = None
        else:
            seed = 0

        #--- DATASET
        self.scenario = CLEAR(
            data_name = config.DATASET_NAME,
            evaluation_protocol=self.EVALUATION_PROTOCOL,
            feature_type=None,
            seed=seed,
            train_transform=self.train_transform,
            eval_transform=self.test_transform,
            dataset_root=self.paths['data'],
            )

    def _define_loss_function(self):
        if self.loss_name == 'cross-entropy':
            self.loss_fn = torch.nn.CrossEntropyLoss()
        elif self.loss_name == 'label-smooth-cross-entropy':
            self.loss_fn = custom.LabelSmoothingCrossEntropy(smoothing=0.1)
        else:
            raise Exception('Unexpected loss specification (%s)!' % self.loss_name)


    def train_eval_on_experiences(self):
        """ Train && Eval on experiences, using the CL strategy 'self.cl_strategy'.
        """
        # TRAINING LOOP
        logger.info("Starting experiment...")
        self.results = []
        logger.info("Current protocol : %r" % self.EVALUATION_PROTOCOL)
        losses = []
        n_train = len(self.scenario.train_stream)
        for index, experience in enumerate(self.scenario.train_stream):
            logger.info('+++++++++++++++++++++++++++++++++++++++++++++++++++')
            logger.info(ccl.Gn + "Start of experience (%d/%d)" % (
                experience.current_experience, n_train-1) + ccl.W)
            logger.info("Current Classes: %r" % experience.classes_in_this_experience)
            train_loader = get_dataset(self.scenario.train_stream,
                index, self.HPARAM)
            #train_stream = self.scenario.streams['train'][index]
            #assert experience == train_stream[index]
            train2(self.cl_strategy, experience)
            losses += self.text_logger.mb_losses
            assert id(self.cl_strategy.model) == id(self.model)  # (*)
            #self.cl_strategy.model = copy.deepcopy(self.model)

            logger.info(ccl.G + "Training completed (%d/%d)" % (index, n_train-1) + ccl.W)
            torch.save(
                self.cl_strategy.model.state_dict(),
                str(self.paths['model'] / f"model{str(index).zfill(2)}.pth")
                )
            logger.info(
                "Computing accuracy on the whole test set with"
                f" {self.EVALUATION_PROTOCOL} evaluation protocol"
                )
            metrics = self.cl_strategy.eval(self.scenario.test_stream)
            logger.info(ccl.G + "Evaluation completed (%d/%d)" % (index, n_train-1) + ccl.W)
            logger.info('>>> Sending experience metrics to W&B ...')
            if self.use_wandb:
                self._wandb_log_metrics(metrics, index, losses)
            self.results.append(metrics)

        # (*): for iCaRL, this 'self.model' is not the same as
        # 'cl_strategy.model', because there are two additional layers:
        #   (train_classifier): Linear(in_features=64, out_features=11, bias=True)
        #   (eval_classifier): NCMClassifier()

    def _wandb_log_metrics(self, metrics, index, losses):
        scraps_md = [
            #{'metric_prefix': 'Top1_Acc_Exp/eval_phase/test_stream',
            #'strip_str': 'Exp',},
            {'metric_prefix': 'Top1_Acc_Stream/eval_phase/test_stream',
            'strip_str': 'Task'},
            {'metric_prefix': 'Loss_Stream/eval_phase/test_stream',
            'strip_str': 'Task',},
            ]
        for scrap_md in scraps_md:
            metric_prefix = scrap_md['metric_prefix']
            strip_str = scrap_md['strip_str']
            logger.info('> Sending to W&B: %s' % metric_prefix)

            top1_acc_keys = [k for k in metrics if k.startswith(metric_prefix)]
            data = []
            for k in top1_acc_keys:
                step = int(k.split('/')[-1].strip(strip_str))
                value = metrics[k]
                data.append([step, value])

            table = wandb.Table(data=data, columns=['task', metric_prefix])
            profile_name = 'exp-%d/%s' % (index, metric_prefix)
            wandb.log({
                profile_name: wandb.plot.line(
                    table, "task", metric_prefix, title=profile_name)})

        # Loss
        logger.info('> updating Loss profile to W&B ...')
        df_loss = pd.DataFrame.from_dict({
            'epoch': 1. + np.arange(len(losses)), 
            'loss': losses
            })
        table = wandb.Table(dataframe=df_loss)
        wandb.log({'loss_profile': 
            wandb.plot.line(table, 'epoch', "loss", title='Loss')
            })


    def _set_misc(self):
        normalize = T.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
            )
        self.train_transform = T.Compose(
            [
                T.Resize(224),
                T.RandomCrop(224),
                T.ToTensor(),
                normalize,
            ]
            )
        self.test_transform = T.Compose(
            [
                T.Resize(224),
                T.CenterCrop(224),
                T.ToTensor(),
                normalize,
            ]
            )

    def _calc_metrics(self):
        # generate accuracy matrix
        num_timestamp = len(self.results)
        accuracy_matrix = np.zeros((num_timestamp, num_timestamp))
        for train_idx in range(num_timestamp):
            for test_idx in range(num_timestamp):
                accuracy_matrix[train_idx][test_idx] = self.results[train_idx][
                    f"Top1_Acc_Stream/eval_phase/test_stream/Task00{test_idx}"]
        logger.info('Accuracy_matrix : \n%r' % accuracy_matrix)
        metric = CLEARMetric().get_metrics(accuracy_matrix)
        logger.info(metric)

        self.metrics = {
            'accuracy_matrix': accuracy_matrix,
            'metric': metric,
            }

    def _save_metrics(self):
        #--- save to json
        """
        metric_log = open(self.paths['dir_dst'] / "metric_log.txt", "w+")
        metric_log.write(
            f"Protocol: {self.EVALUATION_PROTOCOL} "
            f"Seed: {self.seed} "
        )
        json.dump(self.metrics['accuracy_matrix'].tolist(), metric_log, indent=6)
        json.dump(self.metrics['metric'], metric_log, indent=6)
        metric_log.close()
        """

        #--- save to W&B
        if self.use_wandb:
            n_experiences = self.metrics['accuracy_matrix'].shape[1]
            # save accuracy matrix
            df_acc_matrix = pd.DataFrame(self.metrics['accuracy_matrix'],
                columns = ['test-exp-%0d' % i for i in range(n_experiences)],
                index = ['train-exp-%0d' % i for i in range(n_experiences)],
                )
            table = wandb.Table(dataframe=df_acc_matrix)
            logger.info('Sending accuracy matrix to W&B ...')
            wandb.log({'accuracy_matrix': table})

            # save CLEAR metrics
            wandb.log({'clear_metrics': self.metrics['metric']})


#++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++  M A I N  +++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++

def main():

    #--- cli parser
    parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
    '-c', '--fname-config',
    type=str,
    default='./experim/config.yaml',
    help='path to YAML config',
    )
    parser.add_argument(
    '-p', '--fname-procedures',
    type=str,
    default='./experim/procedures.yaml',
    help='path to YAML config',
    )
    args = parser.parse_args()


    #--- global config
    with open(args.fname_config, 'rb') as fi:
        glob_config = yaml.load(fi, Loader=my_yaml_loader)

    dir_dst = glob_config['output']
    os.makedirs(dir_dst, exist_ok=True)
    global logger
    logger = build_logger('%s/runlog.txt' % dir_dst)

    # For CLEAR dataset setup
    logger.info(
        f"This script will train on {config.DATASET_NAME}. "
        "You may change the dataset in config.py."
    )

    HPARAM = glob_config['hyperparams']

    #--- procedure guidelines
    with open(args.fname_procedures, 'rb') as fi:
        procedure_name = glob_config['procedure']
        procedure = yaml.safe_load(fi)[procedure_name]

    # run-config to log
    run_config_to_log = {
        'procedure': procedure,
        'hyperparams': HPARAM,
        }

    # W&B stuff
    wandb_opts = glob_config['wandb']
    wandb_opts.update({'config': run_config_to_log})

    run_mgr = RunMgr(
        hparams = HPARAM,
        model_name = procedure['model'],
        strategy = procedure['strategy'],
        optim_name = procedure['optimizer'],
        loss_name = procedure['loss'],
        plugins = procedure.get('plugins', []),
        scheduler = procedure['scheduler'],
        wandb_opts = wandb_opts,
        dir_dst = dir_dst,
        )
    run_mgr.run()


if __name__ == '__main__':

    main()

#EOF
